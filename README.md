# TI rpmsg-char zerocopy example

This repository contains a minimal zerocopy example demonstrating the use
of the rpmsg_char API to pass shared memory data between a Linux host and
remote M4F or R5F MCU endpoint.

The master branch of the zerocopy example is for customers using Linux kernel
6.6 or later (SDK 10.x). If you are using Linux kernel 5.10 (SDK 8.x) or Linux
kernel 6.1 (SDK 9.x), please use the ti-linux-6.1 branch instead.

The common Linux rpmsg-char based example is located under the "linux" directory.

The "rtos" directory contains the FreeRTOS part of the example for AM64x/R5F, AM62x/M4F
and AM62Ax/C71/R5F.

For more information about the Linux and FreeRTOS parts of this example, please see the
README.md files in the above directories.
