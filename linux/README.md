# TI remoteproc zerocopy example using rpmsg-char

This is a basic zerocopy example application demonstrating the usage
of the rpmsg_char API to pass shared memory data between the host and
remote endpoint, e.g M4F or R5 MCU. The shared memory is managed by
the DMA-BUF Heaps API. We can allocate a buffer from the CMA reserved
heap or from the *"dma-heap-carveout"* heap available in the TI vendor
kernel.

The example runs a number of buffer data exchanges with a remote endpoint.
1. Fill the mmapped shared memory buffer with the specified pattern.

2. Send the buffer physical address, size of the buffer and the pattern
to the remote rpmgs endpoint and start waiting for response.

3. The remote MCU will validate the buffer data with the pattern, invert
the pattern, fill the buffer with the new pattern, and send a response.

4. When the message from the remote endpoint is received, validate the
shared memory with the pattern we got from the remote end.

We're doing a very basic cache coherency management with the help of
*DMA_BUF_IOCTL_SYNC* ioctl before and after accessing the mmapped area.

Important note:
This project requires patches to the Remoteproc CDev driver that have not been
upstreamed at this point. The patches can be found in TI's Linux SDK 10.x,
which also provides the required *<linux/remoteproc_cdev.h>* header. The
required patches are:

* https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/commit/?h=ti-linux-6.6.y&id=eaf3c2016b75b11179859d990969232b9b95add9
* https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/commit/?h=ti-linux-6.6.y&id=a4dac51a55b7950e8e3fdd71db15b8c1686a8a28
* https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/commit/?h=ti-linux-6.6.y&id=cde91a89becee656867b8c8a86e627241dc91449
* https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/commit/?h=ti-linux-6.6.y&id=a00760b55adfddb8cf8104c7b83c0f723ad66870
* https://git.ti.com/cgit/ti-linux-kernel/ti-linux-kernel/commit/?h=ti-linux-6.6.y&id=834f0345f493258e80b7ad2f067888255755c7f7

The zerocopy example currently supports AM62x, AM62Ax, & AM64x, but it can be
easily scaled to support any SoC that supports the rpmsg_char library.

## Build

### Requirements

* GNU Autotools
The package uses the autotools infrastructure for build. The following
versions are used:

    - GNU autoconf : 2.69
    - GNU automake : 1.15.1
    - GNU libtool  : 2.4.6

* TI's Processor SDKs

The Linux SDK contains the needed Linux drivers, while the MCU+ SDK provides
the infrastructure to build the RTOS projects. Customers with specific latency
requirements should evaluate the RT Linux SDK instead of the Linux SDK.

The SDKs and SDK documentation are here:

    - https://www.ti.com/tool/PROCESSOR-SDK-AM62X
    - https://www.ti.com/tool/PROCESSOR-SDK-AM62A
    - https://www.ti.com/tool/PROCESSOR-SDK-AM64X

* The "Remoteproc CDev" driver.

The DMA-heap and DMA-buf user-space APIs allow us to allocate, export
and share a kernel DMA-buf, but they still expose only a generic and very
minimal interface. We must provide the MCU application with the physical
address of the shared memory over a rpmsg channel, and that functionality
is not yet available in these APIs. TI extends the remoteproc cdev kernel
driver to attach the DMA-buf and return the physical address. This is a
temporary solution until there's a proper support in place.

* Shared memory option 1: Add a "dma-heap-reserved" reserved memory in the board devicetree (dts) file.

The TI vendor kernel tree contains additional support for reserving
contiguous memory blocks for use with DMA-heaps. This might be required
for cases where we can't use the CMA reserved range in the MCU application,
or the application requires a specific memory range. The reserved heap
range can be enabled my adding a new "dma-heap-reserved" compatible
node under the "reserved-memory" node. The memory range defined by the
new entry must not conflict with other reserved memory ranges.

Example:

Add a new 32MB range named "apps-shared-memory" at 0xa6000000.
```
reserved-memory {
	...
	apps-shared-memory {
		compatible = "dma-heap-carveout";
		reg = <0x00 0xa6000000 0x00 0x2000000>;
		no-map;
	};
 	...
}
```
In this example, the new DMA-heap name ("apps-shared-memory") can be used with
the "-e" parameter to change the default allocation from the "reserved" DMA-heap
range to the 0xa6000000-0xa7ffffff range.

* Shared memory option 2: Use the CMA heap

Some EVM devicetree files have a reserved memory entry for CMA
(`linux,cma-default`), while others do not. If the dts file for your EVM board
does not reserve a memory entry for CMA, you can still reserve a range for CMA
by using the `cma` kernel command line parameter. For example, running these
commands in uboot would reserve a 256MB memory region for CMA, starting from
0xC000_0000:
```
=> env set optargs cma=256M@3G
=> boot
```

* Using the M4F (AM62x & AM64x only)

The M4F MCU requires a RAT (Region-based Address Translation) entry
to access certain memory ranges. This sample adds such direct mapping
RAT entries for the A000_0000:AFFF_FFFF and C000_0000:CFFF_FFFF memory
ranges. The M4F sample assumes the carveout heap will be located in the
first range while the CMA heap will be located in the second one.

### Set up the Linux infrastructure

The Linux SDK 10.x already has the updated Remoteproc CDev driver. That means
that the default filesystem image provided with the Linux SDK does not need to
rebuild the kernel to add support for shared memory regions.

However, if a "dma-heap-reserved" reserved memory region was added to the board
dts file, then the board dts file needs to be rebuilt and copied to the EVM's
filesystem image.

The TI "Linux Kernel User’s Guide" describes how to build and install
the kernel image, the board dts and the modules on SD card.

    - https://software-dl.ti.com/processor-sdk-linux/esd/AM62X/10_00_07_04/exports/docs/linux/Foundational_Components_Kernel_Users_Guide.html
    - https://software-dl.ti.com/processor-sdk-linux/esd/AM62AX/10_00_00/exports/docs/linux/Foundational_Components_Kernel_Users_Guide.html
    - https://software-dl.ti.com/processor-sdk-linux/esd/AM64X/10_00_07_04/exports/docs/linux/Foundational_Components_Kernel_Users_Guide.html

We also have to make `<linux/remoteproc_cdev.h>` available to user-space code.
Do this by copying the header file into the SDK's sysroots folder.

Example:
```
$ cd <SDK_INSTALL_PATH>
$ cp board-support/ti-linux-kernel-<version>/include/uapi/linux/remoteproc_cdev.h linux-devkit/sysroots/aarch64-oe-linux/usr/include/linux/
```

### Build Steps

- Navigate to the zerocopy project's Linux folder
```
$ cd <RPMSG_CHAR_ZEROCOPY_PATH>/linux
```

- Source the SDK environment
```
$ . <SDK_INSTALL_PATH>/linux-devkit/environment-setup
```

- Generate the configure and build files
```
$ autoreconf --install
```

- Run ./configure
```
$ ./configure --host=aarch64-linux-gnu-gcc
```

- Build the example
```
$ make
```

## Program Usage

```
Usage: ./rpmsg_char_zerocopy <options>
        [-r <rproc_id>]: remote processor id - refer to https://git.ti.com/cgit/rpmsg/ti-rpmsg-char/tree/include/rproc_id.h
        [-n <num_msgs>]: number of zerocopy messages to exchange
        [-d <rpmsg_dev_name>]: rpmsg device name
        [-p <remote_endpt>]: remote end-point address of the rpmsg device
        [-e <dma_heap_name>]: /dev/dma-heap/<dma_heap_name>
        [-s <dma_buf_size>]: dma-buf size in KB
        [-t <pattern>]: 32-bit pattern to fill the dma-buf
        [-h]: usage

Defaults:
        rproc_id: 0
        num_msgs: 1
        rpmsg_dev_name: NULL (rpmsg_char)
        remote_endpt: 16
        dma_heap_name: reserved
        dma_buf_size: 1024
        pattern: 0xaaaa5555
```
## Examples

- Test zerocopy buffer update between M4F core (id=9) and Linux with
128KB buffer, allocated on the cma-reserved dma-heap, with the default
pattern (0xaaaa5555):
```
# ./rpmsg_char_zerocopy -r 9 -s 128
```

- Test the zerocopy buffer update between M4F core (id=9) and Linux with
default buffer size (1MB) allocated on carveout dma-heap named "apps-shared-memory",
with the default pattern:
```
# ./rpmsg_char_zerocopy -r 9 -e apps-shared-memory
```

- Run 10 buffer updates between R5F0-0 core (id=2) and Linux with default
buffer size (1MB) allocated on carveout dma-heap named "apps-shared-memory",
with pattern 0x01010101:
```
# ./rpmsg_char_zerocopy -r 2 -e apps-shared-memory -t 0x01010101 -n 10
```

Example output:
```
root@am64xx-evm:~# ./rpmsg_char_zerocopy -r 2 -e apps-shared-memory -t 0x00aa00aa
Created endpt device rpmsg-char-2-1845, fd = 3 port = 1024
Exchanging 1 messages with rpmsg device on rproc id 2 ...

dma-buf address: 0xa6000000
Buffer @0xffffa3400000 (size 1048576) filled with pattern 0x00aa00aa
Sending buffer data #0
        Address: 0xa6000000
        Size: 1048576
        Pattern: 0x00aa00aa
Receiving buffer data #0: 
        Address: 0xa6000000
        Size: 1048576
        Pattern: 0xff55ff55
Buffer @0xffffa3400000 (size 1048576) successfully validated with pattern 0xff55ff55

Completed 1 buffer updates successfully on rpmsg-char-2-1845

TEST STATUS: PASSED
```
