/*
 * rpmsg_char_zerocopy.c
 *
 * Simple Example application using rpmsg-char library for big
 * data exchange.
 *
 * Copyright (c) 2022 Texas Instruments Incorporated - https://www.ti.com
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/dma-buf.h>
#include <linux/dma-heap.h>
#include <linux/remoteproc_cdev.h>
#include <linux/rpmsg.h>
#include <ti_rpmsg_char.h>

#define NUM_ITERATIONS		1
#define REMOTE_ENDPT		16

/* Application specific defines */
#define DEFAULT_BUF_SIZE_KB	(1024)      /*  1MB */
#define MAX_BUF_SIZE_KB		(32 * 1024) /* 32MB */
#define DEFAULT_BUF_PATTERN	0xAAAA5555
#define DMA_HEAP_RESERVED	"reserved"   /* cma-reserved */

/* Remote endpoint descriptor for the buffer */
struct __attribute__((__packed__)) ipc_buf {
	uint32_t addr;    /* Physical address of the allocated dma-buf */
	uint32_t size;    /* Size of the buffer */
	uint32_t pattern; /* 32-bit pattern */
};

/* Local (host) descriptor of the buffer */
struct local_buf {
	int rproc_fd;         /* file descriptor of opened remoteproc cdev */
	int dma_buf_fd;       /* dma-buf file descriptor */
	uint32_t *shared_buf; /* mmaped dma-buf */
	int size;             /* dma-buf size */
};

/* Send message to remote endpoint. */
int send_msg(int fd, void *msg, int len)
{
	int ret = 0;

	ret = write(fd, msg, len);
	if (ret < 0) {
		perror("Can't write to rpmsg endpt device\n");
		return -1;
	}

	return ret;
}

/* Receive message from remote endpoint. */
int recv_msg(int fd, int len, void *reply_msg, int *reply_len)
{
	int ret = 0;

	/* Note: len should be max length of response expected */
	ret = read(fd, reply_msg, len);
	if (ret < 0) {
		perror("Can't read from rpmsg endpt device\n");
		return -1;
	} else {
		*reply_len = ret;
	}

	return 0;
}

/* Get the dma-heap buffer physical address from remoteproc cdev */
int dmabuf_get_phys(int rproc_fd, int dma_buf_fd, u_int64_t *phys_addr)
{
	struct rproc_dma_buf_attach_data data = {
		.fd = dma_buf_fd,
	};
	int ret;

	ret = ioctl(rproc_fd, RPROC_IOC_DMA_BUF_ATTACH, &data);
	if (ret < 0) {
		printf("ioctl DMA_BUF_PHYS_IOC_CONVERT failed: -%d\n",
		       errno);
		return ret;
	}

	printf("dma-buf address: 0x%llx\n", data.da);

	*phys_addr = data.da;

	return 0;
}

/* Open /dev/dma-heap/<heap_name>
 * This can be cma-reserved or another carveout heap we have in the dts.
 */
int dmaheap_open(char *heap_name)
{
	char name[100];
	int ret;

	snprintf(name, sizeof(name), "/dev/dma_heap/%s", heap_name);
	ret = open(name, O_RDWR);
	if (ret < 0)
		printf("Failed to open %s: -%d\n", name, errno);

	return ret;
}

/* Allocate dma-buf and return its file descriptor */
int dmaheap_alloc(int fd, size_t len)
{
	struct dma_heap_allocation_data data = {
		.fd_flags = O_CLOEXEC | O_RDWR,
		.len = len,
	};
	int ret;

	ret = ioctl(fd, DMA_HEAP_IOCTL_ALLOC, &data);
	if (ret < 0) {
		printf("ioctl DMA_HEAP_IOCTL_ALLOC failed with size %ld: -%d\n",
		       len, errno);
		return ret;
	}

	return data.fd;
}

/* Allocate dma-buf from dma-heap, mmap locally, get the phys address
 * and setup the buffer data we want to pass to the remote endpoint
 * in ipc_buf and the local data we want to keep in local_buf.
 */
int dmabuf_heap_alloc(int rproc_id, int dma_heap_fd, int size,
		       struct local_buf *lbuf,
		       struct ipc_buf *ibuf)
{
	void *shared_buf = NULL;
	uint64_t phys_addr;
	int ret, dma_buf_fd, rproc_fd;
	char rproc_name[32] = { 0 };

	dma_buf_fd = dmaheap_alloc(dma_heap_fd, size);
	if (dma_buf_fd < 0)
		return -1;

	sprintf(rproc_name, "/dev/remoteproc%d", rproc_id);
	rproc_fd = open(rproc_name, O_RDONLY | O_CLOEXEC);
	if (rproc_fd < 0) {
		printf("Failed to open %s: -%d\n", rproc_name, errno);
		return -1;
	}

	ret = dmabuf_get_phys(rproc_fd, dma_buf_fd, &phys_addr);
	if (ret < 0)
		return ret;

	if (phys_addr > ~0UL) {
		printf("Can't pass buffer @%llx (64-bit adress) to the remote endpoint.\n",
		       (unsigned long long) phys_addr);
		return -1;
	}

	shared_buf = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED,
			  dma_buf_fd, 0);

	if (shared_buf == MAP_FAILED) {
		printf("Mapping dma-buf failed: -%d\n", errno);
		close(rproc_fd);
		return -1;
	}

	lbuf->rproc_fd = rproc_fd;
	lbuf->dma_buf_fd = dma_buf_fd;
	lbuf->shared_buf = shared_buf;
	lbuf->size = size;

	ibuf->addr = (uint32_t)phys_addr;
	ibuf->size = size;

	return 0;
}

/* Unmap, close the assiciated phys_fd and dma_buf_fd. */
void dmabuf_heap_free(struct local_buf *lbuf)
{
	munmap(lbuf->shared_buf, lbuf->size);
	close(lbuf->rproc_fd);
	close(lbuf->dma_buf_fd);
}

/* Indicate start/end of a map access session.*/
static int dmabuf_sync(int fd, int start_stop)
{
	struct dma_buf_sync sync = {
		.flags = start_stop | DMA_BUF_SYNC_RW,
	};

	return ioctl(fd, DMA_BUF_IOCTL_SYNC, &sync);
}

/* Fill the locally mapped buffer with pattern. */
void buffer_init(struct local_buf *lbuf, uint32_t pattern)
{
	int i;

	dmabuf_sync(lbuf->dma_buf_fd, DMA_BUF_SYNC_START);
	for(i = 0; i < lbuf->size / sizeof(uint32_t); i++)
		lbuf->shared_buf[i] = pattern;
	dmabuf_sync(lbuf->dma_buf_fd, DMA_BUF_SYNC_END);

	printf("Buffer @%p (size %d) filled with pattern 0x%08x\r\n",
	       lbuf->shared_buf, lbuf->size, pattern);
}

/* Validate the locally mapped buffer after it's updated by
 * the remote endpoint with a pattern also set by the remote
 * endpoint.
 */
int buffer_validate(struct local_buf *lbuf, uint32_t pattern)
{
	int i, len = lbuf->size / sizeof(uint32_t);

	dmabuf_sync(lbuf->dma_buf_fd, DMA_BUF_SYNC_START);
	for(i = 0; i < len; i++) {
		if (lbuf->shared_buf[i] != pattern)
			break;
	}
	dmabuf_sync(lbuf->dma_buf_fd, DMA_BUF_SYNC_END);

	if (i < len) {
		printf("Buffer validation error @%p. Expected 0x%08x, found 0x%08x\n",
			&lbuf->shared_buf[i], pattern, lbuf->shared_buf[i]);
		return -1;
	}
	printf("Buffer @%p (size %d) successfully validated with pattern 0x%08x\n",
	       lbuf->shared_buf, lbuf->size, pattern);

	return 0;
}

/* Run a number of buffer data exchanges with a remote endpoint.
 * Send the buffer address, size and the buffer pattern to the
 * remote end and wait for response. The remote MCU will validate
 * the buffer data with the pattern, invert the pattern and fill
 * the buffer with the new pattern. When the message for the remote
 * enpoint is received, validate the buffer with the pattern
 * we got from the remote end.
 */
int rpmsg_buffer_exchange(int rproc_id, char *dev_name, int remote_endpt,
			  int num_msgs, char *heap_name, uint32_t pattern,
			  uint32_t buffer_size)
{
	int ret, i, packet_len, flags = 0;
	char eptdev_name[32] = { 0 };
	rpmsg_char_dev_t *rcdev;
	int dma_heap_fd;
	struct ipc_buf ibuf;
	struct local_buf lbuf;

	/*
	 * Open the remote rpmsg device identified by dev_name and bind the
	 * device to a local end-point used for receiving messages from
	 * remote processor
	 */
	sprintf(eptdev_name, "rpmsg-char-%d-%d", rproc_id, getpid());
	rcdev = rpmsg_char_open(rproc_id, dev_name, RPMSG_ADDR_ANY, remote_endpt,
				eptdev_name, flags);
	if (!rcdev) {
		perror("Can't create an endpoint device");
		return -EPERM;
        }
	printf("Created endpt device %s, fd = %d port = %d\n", eptdev_name,
		rcdev->fd, rcdev->endpt);
	printf("Exchanging %d messages with rpmsg device on rproc id %d ...\n\n",
		num_msgs, rproc_id);

	/* Open the requested dma-heap device */
	dma_heap_fd = dmaheap_open(heap_name);
	if (dma_heap_fd < 0) {
		rpmsg_char_close(rcdev);
		return dma_heap_fd;
	}

	/* Allocate a buffer from the dma-heap */
	ret = dmabuf_heap_alloc(rproc_id, dma_heap_fd, buffer_size, &lbuf, &ibuf);
	if (ret < 0) {
		close(dma_heap_fd);
		rpmsg_char_close(rcdev);
		return ret;
	}

	for (i = 0; i < num_msgs; i++) {
		/* Fill the memory with pattern */
		buffer_init(&lbuf, pattern);
		/* Tell the remote enpoint what pattern to expect */
		ibuf.pattern = pattern;

		printf("Sending buffer data #%d\n", i);
		printf("\tAddress: 0x%08x\n\tSize: %d\n\tPattern: 0x%08x\n",
		       ibuf.addr, ibuf.size, ibuf.pattern);
		ret = send_msg(rcdev->fd, &ibuf, sizeof(ibuf));
		if (ret < 0) {
			printf("send_msg() failed for iteration %d, ret = %d\n",
			       i, ret);
			goto out;
		}

		if (ret != sizeof(ibuf)) {
			printf("bytes written (%d) doesn't match send request (%ld)\n",
				ret, sizeof(ibuf));
			goto out;
		}

		printf("Receiving buffer data #%d: \n", i);
		ret = recv_msg(rcdev->fd, sizeof(ibuf), &ibuf, &packet_len);
		if (ret < 0 || packet_len != sizeof(ibuf)) {
			printf("recv_msg() failed for iteration %d: %d\n",
			       i, ret);
			goto out;
		}
		printf("\tAddress: 0x%08x\n\tSize: %d\n\tPattern: 0x%08x\n",
		       ibuf.addr, ibuf.size, ibuf.pattern);

		/* Verify if the MCU inverted the pattern.*/
		if (ibuf.pattern & pattern) {
			printf("Received unexpected pattern 0x%08x\n",
			       ibuf.pattern);
			goto out;
		}
		/* Verify data integrity */
		ret = buffer_validate(&lbuf, ibuf.pattern);
		if (ret < 0)
			goto out;
	}

	printf("\nCompleted %d buffer updates successfully on %s\n\n",
		num_msgs, eptdev_name);

out:
	/* Free the local buffer resources */
	dmabuf_heap_free(&lbuf);
	/* Close the dma-heap */
	close(dma_heap_fd);
	/* Close the rpmsg_char device */
	rpmsg_char_close(rcdev);

	return ret;
}

void usage(char *app_name)
{
	printf("\nUsage: %s <options>"
	       "\n\t[-r <rproc_id>]: remote processor id"
	       "\n\t[-n <num_msgs>]: number of zerocopy messages to exchange"
	       "\n\t[-d <rpmsg_dev_name>]: rpmsg device name"
	       "\n\t[-p <remote_endpt>]: remote end-point address of the rpmsg device"
	       "\n\t[-e <dma_heap_name>]: /dev/dma-heap/<dma_heap_name>"
	       "\n\t[-s <dma_buf_size>]: dma-buf size in KB"
	       "\n\t[-t <pattern>]: 32-bit pattern to fill the dma-buf"
	       "\n\t[-h]: usage\n",
	       app_name);
	printf("\nDefaults:"
	       "\n\trproc_id: 0"
	       "\n\tnum_msgs: %d"
	       "\n\trpmsg_dev_name: NULL (rpmsg_char)"
	       "\n\tremote_endpt: %d"
	       "\n\tdma_heap_name: %s"
	       "\n\tdma_buf_size: %d"
	       "\n\tpattern: 0x%08x\n",
		NUM_ITERATIONS, REMOTE_ENDPT, DMA_HEAP_RESERVED,
	        DEFAULT_BUF_SIZE_KB, DEFAULT_BUF_PATTERN);
}

int main(int argc, char *argv[])
{
	char *dev_name = NULL;
	int ret, status, c;
	int rproc_id = 0;
	int num_msgs = NUM_ITERATIONS;
	int remote_endpt = REMOTE_ENDPT;
	char *heap_name = DMA_HEAP_RESERVED;
	uint32_t pattern = DEFAULT_BUF_PATTERN;
	uint32_t size = DEFAULT_BUF_SIZE_KB;

	while (1) {
		c = getopt(argc, argv, "r:n:p:d:e:t:s:h");
		if (c == -1)
			break;

		switch (c) {
		case 'r':
			rproc_id = atoi(optarg);
			break;
		case 'n':
			num_msgs = atoi(optarg);
			break;
		case 'p':
			remote_endpt = atoi(optarg);
			break;
		case 'd':
			dev_name = optarg;
			break;
		case 't':
			pattern = strtoul(optarg, NULL, 16);
			break;
		case 's':
			size = atoi(optarg);
			break;
		case 'e':
			heap_name = optarg;
			break;
		case 'h':
		default:
			usage(argv[0]);
			exit(0);
		}
	}

	if (rproc_id < 0 || rproc_id >= RPROC_ID_MAX) {
		printf("Invalid rproc id %d, should be less than %d\n",
			rproc_id, RPROC_ID_MAX);
		usage(argv[0]);
		return 1;
	}

	if (size < 1 || size > MAX_BUF_SIZE_KB) {
		printf("Invalid size (%d) in KB, should not be greater than %d\n",
			size, MAX_BUF_SIZE_KB);
		usage(argv[0]);
		return 1;
	}
	/* Use auto-detection for SoC */
	ret = rpmsg_char_init(NULL);
	if (ret) {
		printf("rpmsg_char_init() failed: %d\n", ret);
		return ret;
	}

	status = rpmsg_buffer_exchange(rproc_id, dev_name, remote_endpt,
				       num_msgs, heap_name, pattern,
				       (size * 1024));

	rpmsg_char_exit();

	if (status < 0) {
		printf("TEST STATUS: FAILED\n");
	} else {
		printf("TEST STATUS: PASSED\n");
	}

	return 0;
}
