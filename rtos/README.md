# IPC zerocopy Linux example with rpmsg_char

This is a simple example how to share data between user-space Linux
application running on a A53 CPU and FreeRTOS application running on
M4F/R5 CPU using a shared memory buffer and the rpmsg_char library
acting as simple control channel.

The Linux part of this example fills the shared memory with a pattern
and notifies us over rpmsg. We validate the shared memory, update it
with an inverted pattern and notify the remote Linux endpoint.

This example is similar to the ipc_rpmsg_echo_linux example, with
several key differences:

- We create a single rpmsg endpoint for comminicating with a user-space
Linux rpmsg_char application.

- We exchange binary messages over the rpmsg channel containing a shared
memory descriptor. The shared data is located in the shared memory and
is not exchanged over rpmsg channel.

- We don't exchange any messages with other CPUs.

## Requirements

1. Download and install the MCU+SDK for the selected target.

- AM64x MCU+SDK (08.03.00.18)

How to download and setup the SDK:

https://software-dl.ti.com/mcu-plus-sdk/esd/AM64X/08_03_00_18/exports/docs/api_guide_am64x/SDK_DOWNLOAD_PAGE.html

Follow this link to learn more about the AM64x MCU+SDK:

https://software-dl.ti.com/mcu-plus-sdk/esd/AM64X/08_03_00_18/exports/docs/api_guide_am64x/index.html

- AM62x MCU+SDK (08.03.00.07)

How to download and setup the SDK:

https://software-dl.ti.com/mcu-plus-sdk/esd/AM62X/08_03_00_07/exports/docs/api_guide_am62x/SDK_DOWNLOAD_PAGE.html

Follow this link to learn more about the AM62x MCU+SDK:

https://software-dl.ti.com/mcu-plus-sdk/esd/AM62X/08_03_00_07/exports/docs/api_guide_am62x/index.html

2. Validate the SDK setup.

Make sure the installed SDK can build the provided *"Hello World"* example.

https://software-dl.ti.com/mcu-plus-sdk/esd/AM62X/08_03_00_07/exports/docs/api_guide_am62x/GETTING_STARTED_BUILD.html

## Build

This document provides information how to build the example with Makefiles.

More information how to use Makefiles can be found here:

https://software-dl.ti.com/mcu-plus-sdk/esd/AM62X/08_03_00_07/exports/docs/api_guide_am62x/MAKEFILE_BUILD_PAGE.html

or

https://software-dl.ti.com/mcu-plus-sdk/esd/AM64X/08_03_00_18/exports/docs/api_guide_am64x/MAKEFILE_BUILD_PAGE.html

1. Download the example sources into a standalone directory - e.g `ipc_rpmsg_zerocopy_linux`.

2. Set `MCU_PLUS_SDK_PATH` to the MCU+SDK install path and run `gmake` for the selected targets as described below.
This assumes the SDK is installed in directory under ${HOME}/ti.

- am62xx-sk
```
$ cd ipc_rpmsg_zerocopy_linux
$ export MCU_PLUS_SDK_PATH=${HOME}/ti/mcu_plus_sdk_am62x_08_03_00_07
$ gmake -s -C am62x-sk/system_freertos all
```

- am64xx-evm
```
$ cd ipc_rpmsg_zerocopy_linux
$ export MCU_PLUS_SDK_PATH=${HOME}/ti/mcu_plus_sdk_am64x_08_03_00_18
$ gmake -s -C am64x-evm/system_freertos all
```

# Install on a AM6xx-SK kit running Linux

The resulting binary file must be copied in the /lib/firmware directory on AM6xx-SK running Linux.

You can user the remoteproc interface to stop and restart the example without rebooting the board.

Example for AM62x + M4F (am62-mcu-m4f0_0-fw):

```
root@am62xx-evm:~# echo stop > /sys/class/remoteproc/remoteproc0/state
[705989.915914] remoteproc remoteproc0: stopped remote processor 5000000.m4fss

root@am62xx-evm:~# echo start > /sys/class/remoteproc/remoteproc0/state
[705997.926834] remoteproc remoteproc0: powering up 5000000.m4fss
[705997.979930] remoteproc remoteproc0: Booting fw image am62-mcu-m4f0_0-fw, size 86744
[705997.997454]  remoteproc0#vdev0buffer: assigned reserved memory node m4f-dma-memory@9cb00000
[705998.008612] virtio_rpmsg_bus virtio0: rpmsg host is online
[705998.009620] virtio_rpmsg_bus virtio0: creating channel rpmsg_chrdev addr 0x10
[705998.014947]  remoteproc0#vdev0buffer: registered virtio0 (type 7)
[705998.028213] remoteproc remoteproc0: remote processor 5000000.m4fss is now up
root@am62xx-evm:~#
```

More about IPC on AM62x:

https://software-dl.ti.com/processor-sdk-linux/esd/AM62X/08_03_00_19/exports/docs/linux/Foundational_Components_IPC62x.html

More about IPC on AM64x:

https://software-dl.ti.com/processor-sdk-linux/esd/AM64X/latest/exports/docs/linux/Foundational_Components_IPC64x.html

