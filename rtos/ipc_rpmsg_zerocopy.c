/*
 *  Copyright (C) 2021-2022 Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <kernel/dpl/CacheP.h>
#include <kernel/dpl/ClockP.h>
#include <kernel/dpl/DebugP.h>
#include <kernel/dpl/TaskP.h>
#include <drivers/ipc_notify.h>
#include <drivers/ipc_rpmsg.h>
#include "ti_drivers_open_close.h"
#include "ti_board_open_close.h"

/* This example shows message exchange bewteen Linux and RTOS/NORTOS cores.
 *
 * The Linux core initiates IPC with other core's by sending it a message.
 * The other cores echo the same message to the Linux core.
 */

/*
 * Remote core service end point
 *
 * pick any unique value on that core between 0..RPMESSAGE_MAX_LOCAL_ENDPT-1
 * the value need not be unique across cores
 *
 * The service names MUST match what linux is expecting
 */
/* This is used to run the echo test with user space */
#define IPC_RPMESSAGE_SERVICE_CHRDEV      "rpmsg_chrdev"
#define IPC_RPMESSAGE_ENDPT_CHRDEV_PING   (16U)

/*
 * Number of RP Message "servers" we will start,
 * - just one for messages from linux "user space" client using "rpmsg char"
 */
#define IPC_RPMESSAGE_NUM_RECV_TASKS      (1u)

/* RPMessage object used to recvice messages */
RPMessage_Object gIpcRecvMsgObject[IPC_RPMESSAGE_NUM_RECV_TASKS];

/* RPMessage object used to send messages to other non-Linux remote cores */
RPMessage_Object gIpcAckReplyMsgObject;

/* Task priority, stack, stack size and task objects, these MUST be global's */
#define IPC_RPMESSAFE_TASK_PRI         (8U)
#if defined (SOC_AM62AX)
#define IPC_RPMESSAGE_TASK_STACK_SIZE  (32*1024U)
#else
#define IPC_RPMESSAGE_TASK_STACK_SIZE  (8*1024U)
#endif
uint8_t gIpcTaskStack[IPC_RPMESSAGE_NUM_RECV_TASKS][IPC_RPMESSAGE_TASK_STACK_SIZE] __attribute__((aligned(32)));
TaskP_Object gIpcTask[IPC_RPMESSAGE_NUM_RECV_TASKS];

/* non-Linux cores that exchange messages among each other */
#if defined (SOC_AM64X)
uint32_t gRemoteCoreId[] = {
    CSL_CORE_ID_R5FSS0_0,
    CSL_CORE_ID_R5FSS0_1,
    CSL_CORE_ID_R5FSS1_0,
    CSL_CORE_ID_R5FSS1_1,
    CSL_CORE_ID_M4FSS0_0,
    CSL_CORE_ID_MAX /* this value indicates the end of the array */
};
#endif

#if defined (SOC_AM62X)
uint32_t gRemoteCoreId[] = {
    CSL_CORE_ID_M4FSS0_0,
    CSL_CORE_ID_MAX /* this value indicates the end of the array */
};
#endif

#define DebugP_LOG_ENABLED   1

/* Remote endpoint descriptor for the buffer */
struct __attribute__((__packed__)) ipc_buf {
    uint32_t addr;    /* Physical address of the allocated dma-buf */
    uint32_t size;    /* Size of the buffer */
    uint32_t pattern; /* 32-bit pattern */
};

/* Fill buffer with pattern */
void buffer_init(struct ipc_buf *ibuf)
{
    uint32_t *buf_data = (uint32_t *)ibuf->addr;
    int i;

    for(i = 0; i < ibuf->size / sizeof(uint32_t); i++)
        buf_data[i] = ibuf->pattern;

    /* No effect on M4F */
    CacheP_wb(buf_data, ibuf->size, CacheP_TYPE_ALLD);

    DebugP_log("Buffer @0x%08x (size %d) filled with pattern 0x%08x\r\n",
               ibuf->addr, ibuf->size, ibuf->pattern);
}

/* Validate a received buffer with an inverted pattern */
int32_t buffer_validate(struct ipc_buf *ibuf)
{
    int i;
    uint32_t *buf_data = (uint32_t *)ibuf->addr;

    /* No effect on M4F */
    CacheP_inv(buf_data, ibuf->size, CacheP_TYPE_ALLD);

    for(i = 0; i < ibuf->size / sizeof(uint32_t); i++)
    {
        if (buf_data[i] != ibuf->pattern)
        {
            DebugP_log("Buffer validation error @0x%08x. Expected 0x%08x, found 0x%08x\r\n",
                       &buf_data[i], ibuf->pattern, buf_data[i]);
            return SystemP_FAILURE;
        }
    }
    DebugP_log("Buffer @0x%08x (size %d) successfully validated with pattern 0x%08x\r\n",
               ibuf->addr, ibuf->size, ibuf->pattern);
    return SystemP_SUCCESS;
}

void ipc_recv_task_main(void *args)
{
    int32_t status;
    struct ipc_buf ibuf = {0};
    uint16_t recvMsgSize, remoteCoreId;
    uint32_t remoteCoreEndPt;
    RPMessage_Object *pRpmsgObj = (RPMessage_Object *)args;

    DebugP_log("[IPC RPMSG ZEROCOPY] Remote Core waiting for messages at end point %d ... !!!\r\n",
        RPMessage_getLocalEndPt(pRpmsgObj)
        );

    /* wait for messages forever in a loop */
    while(1)
    {
        /* Set 'recvMsgSize' to size of recv buffer, after return `recvMsgSize`
         * contains actual size of valid data in recv buffer.
         */
        recvMsgSize = sizeof(ibuf);
        status = RPMessage_recv(pRpmsgObj,
            &ibuf, &recvMsgSize,
            &remoteCoreId, &remoteCoreEndPt,
            SystemP_WAIT_FOREVER);
        DebugP_assert(status==SystemP_SUCCESS);

        /* Validate the message and the buffer we got from Linux.
         * This example does only a basic check of the message size */
        if (recvMsgSize == sizeof(ibuf)) {
            DebugP_log("IPC message: buffer address 0x%08x%, size %d, pattern 0x%08x\r\n",
                       ibuf.addr, ibuf.size, ibuf.pattern);

            if (buffer_validate(&ibuf) == SystemP_SUCCESS) {
                /* Invert the pattern, and update the buffer before returning */
                ibuf.pattern = ~ibuf.pattern;
                buffer_init(&ibuf);
            }
        } else
            DebugP_log("Invalid IPC message\r\n");

        /* Send back the ipc_buf to sender CPU at the sender endpoint.
         * The Linux side does only a basic check if the pattern is
         * inverted.
         */
        status = RPMessage_send(
            &ibuf, sizeof(ibuf),
            remoteCoreId, remoteCoreEndPt,
            RPMessage_getLocalEndPt(pRpmsgObj),
            SystemP_WAIT_FOREVER);
        DebugP_assert(status==SystemP_SUCCESS);
    }
    /* This loop will never exit */
}

void ipc_rpmsg_create_recv_tasks()
{
    int32_t status;
    RPMessage_CreateParams createParams;
    TaskP_Params taskParams;

    RPMessage_CreateParams_init(&createParams);
    createParams.localEndPt = IPC_RPMESSAGE_ENDPT_CHRDEV_PING;
    status = RPMessage_construct(&gIpcRecvMsgObject[0], &createParams);
    DebugP_assert(status==SystemP_SUCCESS);

    /* We need to "announce" to Linux client else Linux does not know a service exists on this CPU
     * This is not mandatory to do for RTOS clients
     */
    status = RPMessage_announce(CSL_CORE_ID_A53SS0_0, IPC_RPMESSAGE_ENDPT_CHRDEV_PING, IPC_RPMESSAGE_SERVICE_CHRDEV);
    DebugP_assert(status==SystemP_SUCCESS);

    /* Create the tasks which will handle the ping service */
    TaskP_Params_init(&taskParams);
    taskParams.name = "RPMESSAGE_CHAR_ZEROCOPY";
    taskParams.stackSize = IPC_RPMESSAGE_TASK_STACK_SIZE;
    taskParams.stack = gIpcTaskStack[0];
    taskParams.priority = IPC_RPMESSAFE_TASK_PRI;
    /* we use the same task function for echo but pass the appropiate rpmsg handle to it, to echo messages */
    taskParams.args = &gIpcRecvMsgObject[0];
    taskParams.taskMain = ipc_recv_task_main;

    status = TaskP_construct(&gIpcTask[0], &taskParams);
    DebugP_assert(status == SystemP_SUCCESS);
}

void ipc_rpmsg_zerocopy_main(void *args)
{
    int32_t status;

    Drivers_open();
    Board_driversOpen();

    DebugP_log("[IPC RPMSG ZEROCOPY] %s %s\r\n", __DATE__, __TIME__);

    /* This API MUST be called by applications when its ready to talk to Linux */
    status = RPMessage_waitForLinuxReady(SystemP_WAIT_FOREVER);
    DebugP_assert(status==SystemP_SUCCESS);

    /* create message receive tasks, these tasks always run and never exit */
    ipc_rpmsg_create_recv_tasks();

    Board_driversClose();
    /* We dont close drivers since threads are running in background */
    /* Drivers_close(); */
}
